# envcfg

A tiny system for bootstrapping an environment, striking a balance between speed and
convenience. Currently using `zgenom` and `omz` for most of the heavy lifting, and
`fzf` + `ag` + `direnv` for most of the interactive convenience.

To enable debug logging, create `${HOME}/.debug`.

<!-- TODO, add hooks for
1. in .zshrc that will get to the init script
2. assertions, like that we're in zsh and that required binaries like zgenom are present
3. bootstraps zgenom, omz, direnv, etc
5. prepares the environment via direnv
    a. envars
    b. aliases
-->